# Openspeedtest Servers Configs

These are the nginx.conf and docker-compose files for use with launching the Openspeedtest servers:  
https://hebecom.atlassian.net/wiki/spaces/FOUNDTECH/pages/2907245196/H-E-B+s+OpenSpeedTest+Instances

These are running on the following servers:
* inetspeedtest: w2papl0002508.heb.com
* mgmtspeedtest: w2papl0002507.heb.com

The docker-compose.yml file for each server is located in:
```bash
/opt/openspeedtest
```
This file is owned by root with "domain users" as the group.

## Known issue

After the server is patched automatically by TAVM, IPv4 forwarding may be disabled. To fix:
```bash 
sudo nano /etc/sysctl.conf
```

Change `net.ipv4.ip_forward = 0` to `net.ipv4.ip_forward = 1`
Save the file then restart networking, verify change, and restart docker:
```bash
systemctl restart networking
sysctl -p
systemctl restart docker
```

You may have to restart/rebuild the containers themselves.
```bash
cd /opt/openspeedtest
docker compose down
docker compose up -d
```

Logs can be viewed with 
```bash
docker compose logs -f
```
