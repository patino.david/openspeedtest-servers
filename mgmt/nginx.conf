worker_processes  auto;
worker_rlimit_nofile 100000;

events {
    worker_connections  4096;
    multi_accept on;
}

http {
    server {
        listen 80 default_server;
        return 301 https://$host$request_uri;
    }

    server {
        include mime.types;
        default_type application/octet-stream;
        listen 443 ssl;
        server_name inetspeedtest.heb.com;
        ssl_certificate /etc/ssl/private/tls.pem;
        ssl_certificate_key /etc/ssl/private/tls.key;

        client_max_body_size 35m;

        ssl_protocols TLSv1.2 TLSv1.3;
        server_tokens off;

        location /basic_status {
            stub_status;
            access_log off;
            proxy_set_header X-Forwarded-Proto https;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            add_header Strict-Transport-Security "max-age=31536000" always;
            add_header X-XSS-Protection "1; mode=block";
            add_header X-Frame-Options "SAMEORIGIN";
        }

        location / {
            gzip off;
            proxy_pass http://openspeedtest:3000;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
            add_header Strict-Transport-Security "max-age=31536000" always;
            add_header X-XSS-Protection "1; mode=block";
            add_header X-Frame-Options "SAMEORIGIN";
        }

        location /status {
            access_log off;
            proxy_pass http://ost-exporter:9113;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
            add_header Strict-Transport-Security "max-age=31536000" always;
            add_header X-XSS-Protection "1; mode=block";
            add_header X-Frame-Options "SAMEORIGIN";
        }

        location /metrics {
            access_log off;
            proxy_pass http://ost-exporter:9113;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
            add_header Strict-Transport-Security "max-age=31536000" always;
            add_header X-XSS-Protection "1; mode=block";
            add_header X-Frame-Options "SAMEORIGIN";
        }
    }
}